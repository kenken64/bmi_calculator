# bmi_calculator

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## CD CI

```yml
image: openjdk:8-jdk

variables:
  ANDROID_COMPILE_SDK: "28"
  ANDROID_BUILD_TOOLS: "28.0.2"
  ANDROID_SDK_TOOLS:   "4333796"
  FLUTTER_VERSION: "https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.9.1+hotfix.4-stable.tar.xz"

before_script:
  - apt-get --quiet update --yes
  - apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
  - wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
  - unzip -d android-sdk-linux android-sdk.zip
  - echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
  - echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null
  - echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
  - export ANDROID_HOME=$PWD/android-sdk-linux
  - export PATH=$PATH:$PWD/android-sdk-linux/platform-tools/
  # temporarily disable checking for EPIPE error and use yes to accept all licenses
  - set +o pipefail
  - yes | android-sdk-linux/tools/bin/sdkmanager --licenses
  - set -o pipefail
  # flutter sdk setup
  - wget --output-document=flutter-sdk.tar.xz $FLUTTER_VERSION
  - tar -xf flutter-sdk.tar.xz
  - export PATH=$PATH:$PWD/flutter/bin
  - echo flutter.sdk=$PWD/flutter > android/local.properties
  - chmod +x ./android/gradlew

stages:
  - test
  - build
  - deploy

test:android:
  stage: test
  script:
    - cd android
    - ./gradlew -Pci --console=plain :app:testDebug

build:apk:
  stage: build
  script:
    - flutter build apk
  artifacts:
    paths:
      - build/app/outputs/apk

build:bundle:
  stage: build
  script:
    - flutter build appbundle
  artifacts:
    paths:
      - build/app/outputs/bundle

deploy:apk:
  stage: deploy
  script:
    - cd android
    - echo $KEYSTORE_FILE | base64 -d > my.keystore
    - echo $KEYSTORE_PASSWORD
    - echo $KEY_ALIAS
    - echo $KEY_PASSWORD
    - ./gradlew assembleRelease
      -Pandroid.injected.signing.store.file=$(pwd)/my.keystore
      -Pandroid.injected.signing.store.password=$KEYSTORE_PASSWORD
      -Pandroid.injected.signing.key.alias=$KEY_ALIAS
      -Pandroid.injected.signing.key.password=$KEY_PASSWORD
  artifacts:
    paths:
    - app/build/outputs/apk/release
    - CHANGELOG 

test:flutter:
  stage: test
  script:
    - flutter test
```

