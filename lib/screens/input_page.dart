import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/rendering.dart';

import 'package:bmi_calculator/constants.dart';
import 'package:bmi_calculator/components/icon_content.dart';
import 'package:bmi_calculator/components/reusable_card.dart';
import 'package:bmi_calculator/components/bottom_button.dart';
import 'package:bmi_calculator/components/round_icon_button.dart';
import 'results_page.dart';
import 'package:bmi_calculator/calculator_brain.dart';

enum Gender{
  male,
  female,
}

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Gender selectedGender = Gender.male;

  int height = 105;
  int weight = 17;
  int age = 21;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Center( child: Text('BMI CALCULATOR'))
        ),
      body: Column (children: <Widget>
        [
          Expanded(
            child: ReusableCard(
              onPress: (){
                setState((){
                  print ( Gender.male );
                  print ( "MALE card ! ");
                  selectedGender = Gender.male;
                  print ( selectedGender );
                });
              },
              color: selectedGender == Gender.male? kActiveCardColor : kInactiveCardColor,
                cardChild : IconContent(
                  icon: FontAwesomeIcons.mars, 
                  label: 'MALE'
                ),
            ),
          ),
          Expanded(
            child: ReusableCard(
              onPress: (){
                setState((){
                  print ( Gender.female );
                  selectedGender = Gender.female;
                  print ( selectedGender );
                });
              },
              color: selectedGender == Gender.female? kActiveCardColor : kInactiveCardColor,
              cardChild: IconContent(
                icon: FontAwesomeIcons.venus,
                label: 'FEMALE'
              ),
            
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget> [
                Expanded(
                  child: ReusableCard(
                    color: kActiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(children: <Widget>[
                          SizedBox(height: 5.0,)
                        ],),
                        Text(
                          'HEIGHT',
                          style: kLabelTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Text(
                              height.toString(),
                              style: kNumberTextStyle,
                            ),
                            Text(
                              ' cm',
                              style: kLabelTextStyle,
                            ),
                          ],
                        ),
                        SliderTheme(
                          data: SliderTheme.of(context).copyWith(
                            activeTrackColor: Colors.white,
                            inactiveTrackColor: Color(0xFF8D8E98),
                            thumbColor: Color(0xFFEB1555),
                            overlayColor: Color(0x35EB1555),
                            thumbShape: RoundSliderThumbShape(enabledThumbRadius: 10.0),
                            overlayShape: RoundSliderOverlayShape(overlayRadius: 20.0),
                          ),
                          child: Slider(
                              value: height.toDouble(),
                              min: 30.0,
                              max: 250.0,
                              onChanged: (double newValue){
                                setState(() {
                                  height = newValue.round();
                                });
                              },
                            ),
                        )
                      ]
                    )
                  )
                )
              ]
            )
          ),
          Expanded(
            child: Row(
                    children: <Widget>[
                      Expanded(
                        child: ReusableCard(
                          color: kActiveCardColor,
                          cardChild: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'WEIGHT (KG)',
                                style: kLabelTextStyle,
                              ),
                              Text(
                                weight.toString(),
                                style: kNumberTextStyle,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  RoundIconButton(
                                    icon: FontAwesomeIcons.minus,
                                    onPressed: () {
                                      setState(() {
                                        if( weight > 0){
                                          weight--;
                                        }
                                      });
                                    }
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  RoundIconButton(
                                    icon: FontAwesomeIcons.plus,
                                    onPressed: () {
                                      setState(() {
                                        if( weight >= 0){
                                          weight++;
                                        }
                                      });
                                    }
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: ReusableCard(
                              color: kActiveCardColor,
                              cardChild: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                Text(
                                  'AGE',
                                  style: kLabelTextStyle,
                                ),
                                Text(
                                  age.toString(),
                                  style: kNumberTextStyle,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    RoundIconButton(
                                      icon: FontAwesomeIcons.minus,
                                      onPressed: () {
                                        setState(() {
                                          if(age > 0){
                                            age--;
                                          }
                                        });
                                      }
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    RoundIconButton(
                                      icon: FontAwesomeIcons.plus,
                                      onPressed: () {
                                        setState(() {
                                          if(age >=0){
                                            age++;
                                          }
                                      });
                                    }
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          BottomButton(buttonTitle: 'CALCULATE', onTap: () {
              
              CalculatorBrain calc = CalculatorBrain(height: height, weight: weight);
              
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => ResultsPage(
                  bmiResult: calc.calculateBMI(),
                  resultText: calc.getResult(),
                  interpretation: calc.getInterpretation(),
                 ),
              ));
            },
          ),
        ],
      ),
    );
  }
}