import 'package:flutter/material.dart';
import 'package:bmi_calculator/constants.dart';

class IconContent extends StatelessWidget {
  IconContent({this.icon, this.label});

  final IconData icon;
  final String label;

  // MainAxisAlignment - How the children should be placed along the 
  // main axis in a flex layout.
  // SizedBox - A box with a specified size.
  // https://api.flutter.dev/flutter/widgets/SizedBox-class.html
  @override
  Widget build(BuildContext context){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          icon,
          size: 80.0
        ),
        SizedBox(
          height: 10.0
        ),
        Text(
          label,
          style: kLabelTextStyle
        )
      ],
    );
  }
}