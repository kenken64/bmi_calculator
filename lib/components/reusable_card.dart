import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ReusableCard extends StatelessWidget{
  ReusableCard({@required this.color, this.cardChild, this.onPress});

  final Color color;
  final Widget cardChild;
  final Function onPress;

  @override
  Widget build(BuildContext context){
    return SingleChildScrollView( child: GestureDetector(
      onTap: onPress,
      child: Container(
        child: cardChild,
        margin: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            color:color,
            borderRadius: BorderRadius.circular(10.0),
        )
      )
    ));
  }
}