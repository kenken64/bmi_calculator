import 'package:flutter/material.dart';
import 'package:bmi_calculator/constants.dart';
import 'package:flutter/rendering.dart';

class BottomButton extends StatelessWidget {
  BottomButton({@required this.onTap, @required this.buttonTitle});

  final Function onTap;
  final String buttonTitle;

  // GestureDetector
  // https://api.flutter.dev/flutter/widgets/GestureDetector-class.html
  // Creates insets with only the given values non-zero.
  // Insets representation of the borders of a container.
  // Container class is A convenience widget that combines 
  // common painting, positioning, and sizing widgets.
  //  @required to indicate that the parameter is mandatory — that users 
  // must provide a value for the parameter.
  @override
  Widget build(BuildContext context){
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Center(
            child: Text(buttonTitle, style: kLargeButtonTextStyle)
          ),
          color: kBottomContainerColor,
          margin: EdgeInsets.only(top: 10.0),
          padding: EdgeInsets.only(bottom: 10.0),
          width: double.infinity,
          height: kBottomContainerHeight
      ),
    );
  }
}